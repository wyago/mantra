# Mantra, a toy language

<p> <strong>Tiny</strong>&#160; Mantra has a simple data and evaluation model. This simplicity
	makes it trivial to evaluate and understand expressions in your head, making debugging and 
	understanding code easy.
</p>
<p> <strong>Visible</strong>&#160; It's impossible to hide state in the Mantra core. Even the 
	equivalent of the function stack is visible in Mantra. It's possible to execute expressions and view every step of the process, pinpointing exactly where a bug takes effect. No longer will 
	you be blocked by a debugger being unable to pinpoint exactly where a function call is coming
	from.
</p>
<p> <strong>Concurrent</strong>&#160; The evaluation model of Mantra allows for a trivial 
	message passing concurrency mechanism. Fibers are global, allowing for total visibility while 
	also allowing for effective concurrency.
</p>

For more information, see <a href="http://wyago.gitlab.io/mantra">the Mantra website</a>.