<html>
<head>
    <title>Mantra</title>
    <link rel=StyleSheet href="base.css" type="text/css">
</head>
<body>
	<div id="title">
        <h1>Mantra <span id="subtitle">Tiny, Visible, Concurrent</span></h1>
	</div>
    <div id="topButtons">
		<a class="topButton" href="/mantra">Intro</a>
		<a class="topButton" href="/mantra/documentation.html">Documentation</a>
		<a class="topButton" href="https://gitlab.com/wyago/mantra">Repository</a>
	</div>
    <div id="mainPane">
		<p> <strong>Tiny</strong>&#160; Mantra has a simple and local evaluation model. This simplicity
			makes it trivial to evaluate and understand expressions in your head, making debugging and 
			understanding code easy.
		</p>
		<p> <strong>Visible</strong>&#160; It's impossible to hide state in the Mantra core. Even the 
			equivalent of the function stack is visible in Mantra. It's easy to execute expressions
			and view every step of the process, pinpointing exactly where a bug takes effect. It is
			difficult to obfuscate data flow.
		</p>
		<p> <strong>Concurrent</strong>&#160; The evaluation model of Mantra allows for a straightforward 
			message passing concurrency mechanism.
		</p>

		<h2> Example </h2>
		<p>
			The simplest expressions in Mantra can be read as prefix notation,
			though when working with anything deeper than a basic function its
			semantics differ.
		</p>
		<p>
			This is a rewrite rule which duplicates each element within a list, e.g. [1 2 3] becomes [1 1 2 2 3 3].
		</p>
		<pre class="code-block">
repeat [] => [];
repeat [x .. xs] => cat [x x] repeat xs;</pre>
		<p>
			This is how it evaluates:
		</p>
		<pre class="code-block">
repeat [1 2 3]
cat [1 1] repeat [2 3]
cat [1 1] cat [2 2] repeat [3]
cat [1 1] cat [2 2] cat [3 3] repeat []
cat [1 1] cat [2 2] cat [3 3] []
cat [1 1] cat [2 2] [3 3]
cat [1 1] [2 2 3 3]
[1 1 2 2 3 3]</pre>
		<p>
			A small number of rules guide this evaluation process. Mantra is
			a term rewrite system, which means that Mantra uses basic rules to continually rewrite 
			expressions until there are no rules that apply. To do this, you (or a computer)
			can follow these instructions:
		</p>
		<ol>
			<li>Read from right to left until you see a term which is not inside brackets <code>[...]</code>, and isn't a literal value like a number.</li>
			<li>Look up the rewrite rule named by the literal. If it doesn't refer to a rule, halt.</li>
			<li>Attempt to match each of the rule's patterns, in order, with the terms to the right of the literal 
				until one succeeds. If all matches fails, halt.</li>
			<li>Replace the matched terms with the right hand side of the rewrite rule, substituting matched variables.</li>
		</ol>
		<p>
			This is the core mantra evaluation model. Matching and substitution does involve some extra complexity, but the exact
			model used there isn't crucial to the system's properties, and is pretty intuitive to how a person interprets a rewrite rule.
			There is slightly more to it when fibers are involved, but this is all you need to evaluate individual expressions.
		</p>
		<h2> Example improved </h2>
		<p>
			You might have noticed that the stack grew with every expansion of our repeat rule.
			This is because the rule isn't <a href="https://en.wikipedia.org/wiki/Tail_call">tail recursive</a>.
			It turns out that in mantra it's unusually easy to tell whether a rule is tail recursive. If the rule's 
			name is the first literal in the rule definition, then it's tail recursive. Otherwise it
			isn't. This is because the first term in the rule will necessarily be the last evaluated.
		</p>
		<p>
			Let's make the rule tail recursive!
		</p>
		<pre class="code-block">
repeatAcc acc [] => acc;
repeatAcc acc [x .. xs] => repeatAcc cat acc [x x] xs;
repeat => repeatAcc [];</pre>
		<p>
			Now the rule will use constant stack space when evaluating, rather than rapidly growing 
			the stack. This is what evaluation will look like:
		</p>
<pre class="code-block">
repeat [1 2 3]
repeatAcc [] [1 2 3]
repeatAcc cat [] [1 1] [2 3]
repeatAcc [1 1] [2 3]
repeatAcc cat [1 1] [2 2] [3]
repeatAcc [1 1 2 2] [3]
repeatAcc cat [1 1 2 2] [3 3] []
repeatAcc [1 1 2 2 3 3] []
[1 1 2 2 3 3]</pre>
		<p>
			Outside of the context of demonstrating how evaluation works, there's an
			easier way to define this using <code>each</code> and <code>copy</code>,
			which are defined in the prelude.
		</p>
		<pre class="code-block">
repeat => each [copy]

// Where each and copy are defined in the prelude as:
eachAcc a f [] => a;
eachAcc a f [x .. xs] => eachAcc cat a do [unquote f x] f xs;

each => eachAcc [];

copy x => x x;</pre>
		<p>
			Which would evaluate like this:
		</p>
<pre class="code-block">
repeat [1 2 3]
each [copy] [1 2 3]
eachAcc [] [copy] [1 2 3]
eachAcc cat [] do [unquote [copy] 1] [copy] [2 3]
eachAcc cat [] [1 1] [copy] [2 3]
eachAcc [1 1] [copy] [2 3]
eachAcc cat [1 1] do [unquote [copy] 2] [copy] [3]
eachAcc cat [1 1] [2 2] [copy] [3]
eachAcc [1 1 2 2] [copy] [3]
eachAcc cat [1 1 2 2] do [unquote [copy] 3] [copy] []
eachAcc cat [1 1 2 2] [3 3] [copy] []
eachAcc [1 1 2 2 3 3] [copy] []
[1 1 2 2 3 3]
</pre>
		<h2> The REPL </h2>
		<p>
			There are a few special commands on the REPL which all begin with #.
			The most important one is '#load &ltfilename&gt', because you can only declare rules in
			files. If you load the same file more than once, it will reload the rule definitions.
			Declaration files must begin with "version 0" on a line. This is so that future
			interpreters can be backwards compatible without having to bend the language
			to its beginnings.
		</p>
		<p>
			#steps and #slow are useful for debugging. They will toggle whether steps are shown
			and whether the steps are displayed slowly (a 100 ms delay between each step). This
			is useful for debugging, as you can see how your expression evaluates.
		</p>
		<p>
			Lastly, this is a research language: it doesn't have modules, it doesn't have typing,
			any way to interact with the outside world, and it doesn't have any useful libraries or ecosystem whatsoever.
			You probably don't want to do this to accomplish any task other than playing with
			language's semantics themselves.
		</p>
		<p>
			To read more, go to the <a href="/Mantra/documentation.html">documentation</a>, 
			or go ahead and clone it to try it yourself from <a href="https://gitlab.com/wyago/mantra">the repo!</a>
		</p>
	<div id="spacer"> </div>
    </div>
</body>
</html>