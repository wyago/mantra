<html>
<head>
    <title>Mantra - Documentation</title>
    <link rel=StyleSheet href="base.css" type="text/css">
</head>
<body>
	<div id="title">
        <h1>Mantra <span id="subtitle">Tiny, Visible, Concurrent</span></h1>
	</div>
    <div id="topButtons">
		<a class="topButton" href="/">Intro</a>
		<a class="topButton" href="/mantra/documentation.html">Documentation</a>
		<a class="topButton" href="https://gitlab.com/wyago/mantra">Repository</a>
	</div>
    <div id="mainPane">
		<ol>
			<li><a href="#files"> Files </a></li>
			<ol type="i">
			<li><a href="#expressions"> Expressions </a></li>
			<li><a href="#patterns"> Patterns </a></li>
			</ol>
			<li><a href="#fibers"> Fibers and concurrency </a></li>
			<li><a href="#repl"> The REPL </a></li>
			<li><a href="#example"> A slightly more complex example </a></li>
			<li><a href="#rules"> Hard-coded rules </a></li>
		</ol>
		<a name="files"></a>
		<h2> Files </h2>
		<p>
			Mantra files are a list of rules, beginning with a version declaration. At the
			moment the only accepted declaration is "version 0". The version declaration means
			that breaking changes don't require you to update the world. The new interpreter 
			can run the correct version of Mantra per-file, and if there are interop issues
			it can panic on those precise issues rather than breaking the world.
		</p>
		<p>
			Rules are described with a name, a left hand pattern, and a right hand rewrite.
			First is the name, then the pattern to match, then '=>', then the rewrite, then a semicolon
			that ends the rule definition.
		</p>
		
		<a name="expressions"></a>
		<h3> Expressions </h3>
		<p>
			The syntax for rewrite expressions (the right hands of rules) is simple. There are literals,
			lists, numbers, and strings, all of which are terms. Literals are any series of 
			characters that aren't delimiters, where delimiters are white space, '[', ']', '"', and
			 ';'. Lists start with '[', contain any number of terms delimited by white space, and end 
			with ']'. Numbers are any standard floating point notation number and can hold at least
			a double precision float. Strings start with '"', and are made up of every character up
			till the next '"'. Currently there is no character escaping, and strings are treated as 
			long literals.
		</p>
		<p>
			Mantra is a term rewrite system, meaning that instead of having special data structures
			for handling function stacks and heap memory, Mantra is exclusively a set of rules for how 
			terms should be rearranged. This makes evaluating Mantra expressions easy to do 
			in your head.
		</p>
		<ol>
			<li>Read from right to left until you see a literal which is not inside brackets <code>[...]</code>, and isn't a value like a number.</li>
			<li>Look up the rewrite rule named by the literal. If it doesn't refer to a rule, halt.</li>
			<li>Attempt to match each of the rule's patterns, in order, with the terms to the right of the literal 
				until one succeeds. If all matches fails, halt.</li>
			<li>Replace the matched terms with the right hand side of the rewrite rule, substituting matched variables.</li>
		</ol>
		<p>
			The most complicated part of this process is pattern matching, but even that is fairly
			simple. I'll explain the reasoning behind halting on failed matches rather than 
			continuing to process each rule later, when discussing concurrency.
		</p>
		
		<a name="patterns"></a>
		<h3> Patterns </h3>
		<p>
			Patterns are mostly similar to the expressions they represent. The only difference
			is that literals are parameters, and lists which have ".." in them match the terms
			before ".." to the first terms of the matched list, and the single literal after
			the ".." to the remainder of the list. Also, the word "_" ignores the argument,
			binding it to nothing.
		</p>
		<h4> Examples </h4>
		<div class="code-block">math x y => - + x x y;</div>
		<p>
			This example is trivial, but shows the basics. This will effectively replace "math"
			with - and +, duplicating the first argument. This example also helps introduce how 
			easy it is to write code in point-free style:
		</p>
		<div class="code-block">math => - + copy;</div>
		<p>
			In this, copy replaces its one argument with two copies. This doesn't always
			help readability, but it can sometimes be unnecessarily redundant to name
			highly abstract parameters when a function is merely a series of steps.
		</p>
		<div class="code-block">
			at 0 [x .. xs] => x;<br/>
			at n [x .. xs] => at - n 1 xs;
		</div>
		<p>
			This is a more complicated rule which showcases all of the patterns other
			than list matching. When a rule is defined multiple times in a row, Mantra
			attempts to match them in top down order. The first match made is the one that is
			used. This rule rewrites the head of the list argument if the index is zero, and 
			otherwise subtracts one from the index and searches the tail of the list.
		</p>
		
		<a name="fibers"></a>
		<h2> Fibers and concurrency </h2>
		<p>
			Mantra is uniquely suited for concurrency. Because the only data that the program
			holds lies in the program terms themselves, it's easy to organize the memory by thread.
		</p>
		<p>
			The primitive which Mantra uses for concurrency is the fiber. To put it simply, 
			fibers are a list of terms that is actively being evaluated. The REPL, for example,
			spawns a single fiber. When you type in an expression, that fiber's contents are
			replaced with your parsed input and the fiber is evaluated.
		</p>
		<p>
			To make more fibers, you can use the "pass" hard-coded rule. Pass takes two arguments.
			The first is the name of a fiber, which should be a quoted literal e.g. [coolFiber] 
			(avoiding halting). The second is a list of terms to be appended to the end of the fiber. 
			If there isn't a fiber with the given name, it is created with the second argument 
			as its contents. This allows for simple message passing and concurrency.
		</p>
		<p>
			Lets look at an example.
		</p>
		<div class="code-block">
			forever f => forever f unquote f
		</div>
		<div class="code-block">
			pass [adder] [forever [+]]<br/>
			pass [adder] [1 2 3 4 5]<br/>
			showFiber [adder] ==> [forever [+] + 15]
		</div>
		<p>
			This is a trivial example, but it shows the basics of how fibers work. Normally
			you'd probably have a more complex rule repeated by "forever", but in this case
			we're summing the numbers passed to the fiber. Here you can see why fibers
			halt when a rule fails to pattern match. If it didn't halt, then the "forever"
			rule would evaluate perpetually, expanding indefinitely when no numbers
			are being passed to it. Because it halts, it blocks efficiently while it waits
			for more arguments.
		</p>
		
		<a name="repl"></a>
		<h2> The REPL </h2>
		<p>
			The basic operation in the REPL is typing out a Mantra expression, and hitting enter.
			This will evaluate the expression and display its result. If you have #steps
			on, it will show the steps that lead to the answer. This is how I wrote the code examples
			in this document (probably would be a good language for literate programming!).
		</p>
		<p>
			There are a few special commands on the REPL which begin with #.
			The most important one is '#load filename', because you can only 
			declare rules in files. If you load the same file more than once, it will 
			reload the rule definitions.
		</p>
		<p>
			#steps and #slow are useful for debugging. They will toggle whether steps are shown
			and whether the steps are displayed slowly (a 100 ms delay between each step). This
			is incredibly useful for debugging, as you can see precisely how your expression 
			evaluates.
		</p>
		
		<a name="example"></a>
		<h2> A slightly more complex example </h2>
		<p>
			Let's look at a more interesting example than duplicating items in a list.
			We'll implement unbalanced binary search trees in Mantra. This is still simple,
			but gives an idea of how more complex data structures are represented in Mantra.
			To store a binary tree node in mantra, we use a list with the key being 
			its first term, and its left and right children being the following two terms.
			If a child is null, it's represented with the empty list.
		</p>
		
		<p>
			Inserting into this structure is fairly simple. There are effectively two
			cases to consider: inserting into a null node, and inserting into a node
			with its own key.
		</p>
		<pre class="code-block">insert e [] => [e [] []];
insert e [x l r] => choose < e x 
	[do [x insert e l r]]
	[do [x l insert e r]];
</pre>

		<p>
			Now you can create some functions for iterating over the structure. This is
			extremely simple, as you deconstruct the tree and run the given function
			on the keys in the correct order.
		</p>
<pre class="code-block">preorder f [] => ;
preorder f [x l r] => preorder f r preorder f l unquote f x;

inorder f [] => ;
inorder f [x l r] => inorder f r unquote f x inorder f l;</pre>
		
		<a name="rules"></a>
		<h2> Hard-coded rules </h2>
		<p>
			Basic arithmetic is supported (<code>+</code>, 
			<code>-</code>, <code>*</code>, 
			<code>/</code>, and <code>%</code>), as well as 
			random and floor. Random takes no arguments and is replaced by a random number 
			from 0 to 1. Floor takes one argument and replaces it with the closest lower
			integer.
		</p>
		<h3> <code>=</code> and <code>!=</code> </h3>
		<p>
			The <code>=</code> and <code>!=</code> both do deep 
			structural equality tests. Lists must have the exact same elements, numbers 
			must have exactly the same value, and literals must have the exact same name.
		</p>
		<h3> Comparison </h3>
		<p>
			<code>&gt</code>, <code>&lt</code>, <code>&lt=</code>,
			and <code>&gt=</code> all take two numbers as arguments and become 
			<code>[true]</code> or <code>[]</code> if they are valid 
			or not valid respectively.
		</p>
		<h3> <code>cat [...] [...] => [......]</code> </h3>
		<p>
			This takes two lists and concatenates them. In the current
			implementation <code>cat</code> has the best performance when appending
			small lists onto larger ones, rather than putting small lists in front
			of large ones.
		</p>
		<h3> <code>unquote [x y ... z]  => x y ... z</code></h3>
		<p>
			This takes a list an replaces it with its contents. This
			is useful for performance reasons, as unfolding an entire list would take
			n steps, and unquoting is used frequently.
		</p>
		<h3> <code>pass [otherFiber] [terms] => []</code> </h3>
		<p>
			This takes a quoted name and a list of terms. If a fiber of
			the given name doesn't exist, it is created with the list of terms as its 
			contents. If a fiber with that name does exist, the list of terms is 
			appended onto its contents.
		</p>
		<h3> <code>showFiber [otherFiber] => [otherFiberContents]</code> </h3>
		<p>
			This takes a quoted name. It turns into the contents
			of the named fiber, or <code>[]</code> if the named fiber doesn't exist.
		</p>
		<h3> <code>do [+ 1 2] => [3]</code> </h3>
		<p>
			This takes a list or a string. If the argument is a list, it
			is evaluated as if it were a fiber, and turned into the result, still in
			a list. If it's a string then it first parses the string, then acting
			in the same manner upon the list of terms resulting from evaluation. This
			is useful when you want to compute something in a list, but don't know
			how many terms will result from the computation. It wouldn't be possible
			to wrap them back up in a list without knowing how many terms there are.
			With do you can get around the issue by leaving the results in the list
			they came from.
		</p>
		
	<div id="spacer"> </div>
	</div>
</body>
</html>